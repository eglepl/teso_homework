<?php declare(strict_types = 1);

namespace App\Infrastructure\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;

abstract class Type extends \Doctrine\DBAL\Types\Type
{
    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
