<?php declare(strict_types = 1);

namespace App\Infrastructure\DBAL\Types;

use App\Domain\Asset\CurrencyEnum;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class
CurrencyType extends Type
{
    const CURRENCY_TYPE = 'currency_type';

    /**
     * @param array $fieldDeclaration
     * @param AbstractPlatform $platform
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(255)';
    }

    /**
     * @param                  $value
     * @param AbstractPlatform $platform
     *
     * @return CurrencyEnum
     * @throws \ReflectionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): CurrencyEnum
    {
        return new CurrencyEnum($value);
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return string|null
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): ?string
    {
        if ($value instanceof CurrencyEnum) {
            $value = $value->getValue();
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::CURRENCY_TYPE;
    }
}
