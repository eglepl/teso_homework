<?php declare(strict_types = 1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\EntityInterface;

interface SavableInterface
{
    /**
     * @param EntityInterface $object
     */
    public function save(EntityInterface $object): void;
}
