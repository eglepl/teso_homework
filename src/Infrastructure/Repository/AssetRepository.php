<?php declare(strict_types = 1);

namespace App\Infrastructure\Repository;

use App\Domain\Asset\AssetRepositoryInterface;
use App\Domain\Asset\CurrencyEnum;
use App\Domain\Entity\Asset;
use App\Domain\Entity\EntityInterface;
use App\Domain\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Asset|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asset|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asset[]    findAll()
 * @method Asset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AssetRepository extends ServiceEntityRepository implements AssetRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asset::class);
    }

    /**
     * @param User         $user
     * @param string       $label
     * @param CurrencyEnum $currency
     *
     * @return array
     */
    public function findByUserLabelCurrency(User $user, string $label, CurrencyEnum $currency): array
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.user = :userId')
            ->andWhere('a.label = :label')
            ->andWhere('a.currency = :currency')
            ->setParameter('userId', $user->getId())
            ->setParameter('label', $label)
            ->setParameter('currency', $currency->getValue());

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param EntityInterface $asset
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(EntityInterface $asset): void
    {
        $this->_em->remove($asset);
        $this->_em->flush();
    }

    /**
     * @param EntityInterface $asset
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(EntityInterface $asset): void
    {
        $this->_em->persist($asset);
        $this->_em->flush();
    }
}
