<?php declare(strict_types = 1);

namespace App\Infrastructure\Repository;

use App\Domain\Entity\EntityInterface;

interface DeletableInterface
{
    /**
     * @param EntityInterface $object
     */
    public function delete(EntityInterface $object): void;
}
