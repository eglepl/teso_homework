<?php declare(strict_types = 1);

namespace App\Domain\Entity;

use App\Domain\Asset\CurrencyEnum;
use App\Infrastructure\Repository\AssetRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;


/**
 * @ORM\Entity(repositoryClass=AssetRepository::class)
 *
 * @Serializer\VirtualProperty(
 *     name="userId",
 *     exp="object.getUser().getId()",
 *     options={
 *         @Serializer\Type("integer"),
 *         @Serializer\Groups({"Default"})
 *     }
 * )
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Asset implements EntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @OA\Property(
     *     type="integer",
     *     description="Asset id",
     *     readOnly=true
     * )
     * @Serializer\Groups({"Default"})
     * @Serializer\Expose
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @OA\Property(
     *     type="string",
     *     required=true,
     *     maxLength=255,
     *     description="Asset label"
     * )
     * @Serializer\Groups({"Write", "Default"})
     * @Serializer\Expose
     */
    private $label;

    /**
     * @ORM\Column(type="integer")
     *
     * @OA\Property(
     *     type="integer",
     *     description="Minimum number of players in squad"
     * )
     * @Serializer\Groups({"Write", "Default"})
     * @Serializer\Expose
     */
    private $value;

    /**
     * @ORM\Column(type="currency_type")
     *
     * @OA\Property(
     *     type="string",
     *     format="enum",
     *     enum={"BTC", "IOTA", "ETH"},
     *     description="Competitor status"
     * )
     * @Serializer\Groups({"Write", "Default"})
     * @Serializer\Expose
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="assets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return Asset
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int $value
     *
     * @return Asset
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return CurrencyEnum|null
     */
    public function getCurrency(): ?CurrencyEnum
    {
        return $this->currency;
    }

    /**
     * @param CurrencyEnum $currency
     *
     * @return Asset
     */
    public function setCurrency(CurrencyEnum $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return Asset
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
