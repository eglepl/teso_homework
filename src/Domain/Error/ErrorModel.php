<?php declare(strict_types = 1);

namespace App\Domain\Error;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;

class ErrorModel
{
    /**
     * @var int
     *
     * @SWG\Property(
     *     type="integer",
     *     description="Base error status code"
     * )
     */
    private $status;

    /**
     * @var string
     *
     * @SWG\Property(
     *     type="string",
     *     description="Base error message"
     * )
     */
    private $message;

    /**
     * @var array
     *
     * @SWG\Property(
     *      property="errors",
     *      type="array",
     *      description="List of specific errors",
     *      @SWG\Items(
     *          ref=@Model(type=Error::class)
     *      )
     * )
     */
    public $errors = [];

    /**
     * Errors constructor.
     * @param int $status
     * @param string $message
     */
    public function __construct(int $status, string $message)
    {
        $this->status = $status;
        $this->message = $message;
    }

    /**
     * @param array $errors
     *
     * @return ErrorModel
     */
    public function setErrors(array $errors): ErrorModel
    {
        $this->errors = $errors;

        return $this;
    }

    /**
     * @param Error $error
     * @return ErrorModel
     */
    public function addError(Error $error): ErrorModel
    {
        $this->errors[] = $error;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $vars = get_object_vars($this);
        $array = [];
        foreach ($vars as $key => $value) {
            if (\is_array($value)) {
                $newArray = [];
                foreach ($value as $valueItem) {
                    $newArray[] = $valueItem->toArray();
                }
                $array[ltrim($key, '_')] = $newArray;
            } else {
                $array[ltrim($key, '_')] = $value;
            }
        }

        return $array;
    }
}
