<?php declare(strict_types = 1);

namespace App\Domain\Error;

use OpenApi\Annotations as SWG;

class Error
{
    /**
     * @var string
     *
     * @SWG\Property(
     *     type="string",
     *     description="Error message"
     * )
     */
    private $message;

    /**
     * @var string
     *
     * @SWG\Property(
     *     type="string",
     *     description="Error code"
     * )
     */
    private $code;

    /**
     * @var string
     * @SWG\Property(
     *     type="string",
     *     description="Error path"
     * )
     */
    private $path;

    /**
     * Error constructor.
     *
     * @param string   $message
     * @param string   $code
     * @param string   $path
     */
    public function __construct(string $message, string $code, string $path)
    {
        $this->message = $message;
        $this->code = $code;
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $vars = get_object_vars($this);
        $array = [];
        foreach ($vars as $key => $value) {
            $array[ltrim($key, '_')] = $value;
        }

        return $array;
    }
}
