<?php declare(strict_types = 1);

namespace App\Domain\Asset;

use App\Domain\Entity\Asset;
use JMS\Serializer\SerializerInterface;

class AssetListResult
{
    const USD = 'USD';

    /** @var SerializerInterface */
    private $serializer;

    /** @var array */
    private $assets;

    /** @var array */
    private $ratesInUSD;

    /**
     * AssetListResult constructor.
     *
     * @param SerializerInterface $serializer
     * @param array               $assets
     * @param array               $ratesInUSD
     */
    public function __construct(SerializerInterface $serializer, array $assets, array $ratesInUSD)
    {
        $this->serializer = $serializer;
        $this->assets = $assets;
        $this->ratesInUSD = $ratesInUSD;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        $result = [];

        if (empty($this->assets)) {
            return $this->serializer->serialize($result, 'json');
        }
        /** @var Asset $asset */
        foreach ($this->assets as $asset) {
            $result[] = [
                'id' => $asset->getId(),
                $asset->getLabel() => [
                    $asset->getCurrency()->getValue() => $asset->getValue(),
                    self::USD => $asset->getValue() * $this->ratesInUSD[$asset->getCurrency()->getValue()]
                ]
            ];
        }

        return $this->serializer->serialize($result, 'json');
    }
}
