<?php declare(strict_types = 1);

namespace App\Domain\Asset\Create;

use App\Domain\CommandInterface;

class CreateAsset implements CommandInterface
{
    /** @var string */
    private $label;

    /** @var int */
    private $value;

    /** @var string */
    private $currency;

    /**
     * CreateAsset constructor.
     *
     * @param string $label
     * @param int    $value
     * @param string $currency
     */
    public function __construct(string $label, int $value, string $currency)
    {
        $this->label = $label;
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }
}
