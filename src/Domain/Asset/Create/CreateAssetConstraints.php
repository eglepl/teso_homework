<?php declare(strict_types = 1);

namespace App\Domain\Asset\Create;

use App\Application\Validator\ValidatorConstraintsInterface;
use App\Domain\Asset\CurrencyEnum;
use Symfony\Component\Validator\Constraints as Assert;

class CreateAssetConstraints implements ValidatorConstraintsInterface
{

    public function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'label' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Length(['max' => 255]),
            ]),
            'currency' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Length(['max' => 255]),
                new Assert\Choice(CurrencyEnum::getAvailableValues()),
            ]),
            'value' => new Assert\Required([
                new Assert\NotBlank(),
                new Assert\Type("integer"),
                new Assert\Range(["min" => 1]),
            ]),
        ]);
    }
}
