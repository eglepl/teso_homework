<?php declare(strict_types = 1);

namespace App\Domain\Asset\Create;

use App\Application\Exception\UnprocessableEntityException;
use App\Domain\Asset\AssetRepositoryInterface;
use App\Domain\Asset\CurrencyEnum;
use App\Domain\Entity\Asset;
use App\Domain\User\UserProvider;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateAssetHandler implements MessageHandlerInterface
{
    /** @var UserProvider */
    private $userProvider;

    /** @var AssetRepositoryInterface */
    private $assetRepository;

    /**
     * CreateAssetHandler constructor.
     *
     * @param UserProvider             $userProvider
     * @param AssetRepositoryInterface $assetRepository
     */
    public function __construct(UserProvider $userProvider, AssetRepositoryInterface $assetRepository)
    {
        $this->userProvider = $userProvider;
        $this->assetRepository = $assetRepository;
    }

    /**
     * @param CreateAsset $command
     *
     * @throws UnprocessableEntityException
     * @throws \ReflectionException
     */
    public function __invoke(CreateAsset $command): void
    {
        $assets = $this->assetRepository->findByUserLabelCurrency(
            $this->userProvider->getCurrentUser(),
            $command->getLabel(),
            new CurrencyEnum($command->getCurrency())
        );

        if (!empty($assets)) {
            throw new UnprocessableEntityException(
                \sprintf(
                    'Asset already exists with label: %s and currency: %s',
                    $command->getLabel(),
                    $command->getCurrency()
                )
            );
        }

        $newAsset = (new Asset())
            ->setUser($this->userProvider->getCurrentUser())
            ->setValue($command->getValue())
            ->setLabel($command->getLabel())
            ->setCurrency(new CurrencyEnum($command->getCurrency()));

        $this->assetRepository->save($newAsset);
    }
}
