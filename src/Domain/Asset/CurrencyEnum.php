<?php declare(strict_types = 1);

namespace App\Domain\Asset;

use App\Domain\Enum;

class CurrencyEnum extends Enum
{
    const BTC = 'BTC';
    const ETH = 'ETH';
    const IOTA = 'IOTA';

    /**
     * @return string
     */
    protected function getEnumName(): string
    {
        return 'Currency';
    }
}
