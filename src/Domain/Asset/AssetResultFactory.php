<?php declare(strict_types = 1);

namespace App\Domain\Asset;

use JMS\Serializer\SerializerInterface;

class AssetResultFactory
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param array $ratesInUSD
     * @param array $assets
     *
     * @return AssetListResult
     */
    public function buildAssetListResult(array $ratesInUSD, array $assets): AssetListResult
    {
        return new AssetListResult($this->serializer, $assets, $ratesInUSD);
    }
}
