<?php declare(strict_types = 1);

namespace App\Domain\Asset\Delete;

use App\Application\Exception\ResourceNotFoundException;
use App\Domain\Asset\AssetRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class DeleteAssetHandler implements MessageHandlerInterface
{
    /** @var AssetRepositoryInterface */
    private $assetRepository;

    /**
     * DeleteAssetHandler constructor.
     *
     * @param AssetRepositoryInterface $assetRepository
     */
    public function __construct(AssetRepositoryInterface $assetRepository)
    {
        $this->assetRepository = $assetRepository;
    }

    /**
     * @param DeleteAsset $command
     *
     * @throws ResourceNotFoundException
     */
    public function __invoke(DeleteAsset $command): void
    {
        $asset = $this->assetRepository->findOneBy([
            'id' => $command->getId()
        ]);

        if (\is_null($asset)) {
            throw new ResourceNotFoundException('Asset');
        }

        $this->assetRepository->delete($asset);
    }
}
