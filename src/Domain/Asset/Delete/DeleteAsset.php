<?php declare(strict_types = 1);

namespace App\Domain\Asset\Delete;

use App\Domain\CommandInterface;

class DeleteAsset implements CommandInterface
{
    /** @var int */
    private $id;

    /**
     * DeleteAsset constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
