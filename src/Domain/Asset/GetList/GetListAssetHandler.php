<?php declare(strict_types = 1);

namespace App\Domain\Asset\GetList;

use App\Application\Helper\Json;
use App\Domain\Asset\AssetListResult;
use App\Domain\Asset\AssetRepositoryInterface;
use App\Domain\Asset\AssetResultFactory;
use App\Domain\Asset\CurrencyEnum;
use App\Domain\User\UserProvider;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetListAssetHandler implements MessageHandlerInterface
{
    const EXCHANGE_URL = 'https://api.coinpaprika.com/v1/tickers/';
    const BTC_ID = 'btc-bitcoin';
    const IOTA_ID = 'miota-iota';
    const ETH_ID = 'eth-ethereum';

    const COINS_ID = [
        CurrencyEnum::BTC => self::BTC_ID,
        CurrencyEnum::IOTA => self::IOTA_ID,
        CurrencyEnum::ETH => self::ETH_ID
    ];

    /** @var AssetRepositoryInterface */
    private $assetRepository;

    /** @var UserProvider */
    private $userProvider;

    /** @var AssetResultFactory */
    private $assetResultFactory;

    /**
     * GetListAssetHandler constructor.
     *
     * @param AssetRepositoryInterface $assetRepository
     * @param UserProvider             $userProvider
     * @param AssetResultFactory       $assetResultFactory
     */
    public function __construct(
        AssetRepositoryInterface $assetRepository,
        UserProvider $userProvider,
        AssetResultFactory $assetResultFactory
    ) {
        $this->assetRepository = $assetRepository;
        $this->userProvider = $userProvider;
        $this->assetResultFactory = $assetResultFactory;
    }

    /**
     * @param GetListAsset $command
     *
     * @return AssetListResult
     * @throws \App\Application\Exception\ResourceNotFoundException
     */
    public function __invoke(GetListAsset $command): AssetListResult
    {
        $ratesInUSD = [];

        foreach (self::COINS_ID as $coin => $id) {
            $ratesInUSD[$coin] = $this->getExchangeRateInUSD($id);
        }

        $assets = $this->assetRepository->findBy([
            'user' => $this->userProvider->getCurrentUser()
        ]);

        return $this->assetResultFactory->buildAssetListResult($ratesInUSD, $assets);
    }

    private function getExchangeRateInUSD(string $coinId): float
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::EXCHANGE_URL . $coinId);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec($ch);

            /** @var array $data */
            $data = JSON::decode($response, true);

            if (\array_key_exists('quotes', $data)) {
                if (\array_key_exists('USD', $data['quotes'])) {
                    if (\array_key_exists('price', $data['quotes']['USD'])) {
                        return $data['quotes']['USD']['price'];
                    }
                }
            }

            return 0.0;
        } catch (\Throwable $e) {
            return 0.0;
        }
    }
}
