<?php declare(strict_types = 1);

namespace App\Domain\Asset;

use App\Domain\Entity\Asset;
use App\Domain\Entity\User;
use App\Infrastructure\Repository\DeletableInterface;
use App\Infrastructure\Repository\SavableInterface;

/**
 * @method Asset|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asset|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asset[]    findAll()
 * @method Asset[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
interface AssetRepositoryInterface extends SavableInterface, DeletableInterface
{
    /**
     * @param User         $user
     * @param string       $label
     * @param CurrencyEnum $currency
     *
     * @return User|null
     */
    public function findByUserLabelCurrency(User $user, string $label, CurrencyEnum $currency): array;
}
