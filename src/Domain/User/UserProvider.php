<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Application\Exception\ResourceNotFoundException;
use App\Domain\Entity\User;
use Symfony\Component\Security\Core\Security;

class UserProvider
{
    /** @var Security */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function getCurrentUser(): User
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if (empty($user)) {
            throw new ResourceNotFoundException('Current user');
        }

        return $user;
    }
}
