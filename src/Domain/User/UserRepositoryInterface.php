<?php declare(strict_types=1);

namespace App\Domain\User;

use App\Infrastructure\Repository\SavableInterface;

interface UserRepositoryInterface extends SavableInterface
{
}
