<?php declare(strict_types = 1);

namespace App\Application\Controller;

use App\Application\Validator\ValidatorInterface;
use App\Domain\CommandInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class ApiController extends AbstractController
{
    use HandleTrait;

    /** @var MessageBusInterface */
    private $messageBus;

    /** @var ValidatorInterface */
    protected $validator;

    /**
     * ApiController constructor.
     *
     * @param MessageBusInterface $messageBus
     * @param ValidatorInterface  $validator
     */
    public function __construct(MessageBusInterface $messageBus, ValidatorInterface $validator)
    {
        $this->messageBus = $messageBus;
        $this->validator = $validator;
    }

    /**
     * @param CommandInterface $message
     *
     * @return mixed
     */
    protected function handleCommand(CommandInterface $message)
    {
        return $this->handle($message);
    }
}
