<?php declare(strict_types = 1);

namespace App\Application\Controller;

use App\Application\Helper\Json;
use App\Domain\Asset\Create\CreateAsset;
use App\Domain\Asset\Create\CreateAssetConstraints;
use App\Domain\Asset\Delete\DeleteAsset;
use App\Domain\Asset\GetList\GetListAsset;
use App\Domain\Entity\Asset;
use App\Domain\Error\ErrorModel;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * @OA\Tag(name="Match", description="Match API")
 */

class AssetController extends ApiController
{

    /**
     * List all assets.
     *
     * @OA\Response(
     *     response=200,
     *     description="List of assets"
     * )
     *
     * @return Response
     * @Security(name="Authorization")
     */
    public function getList(): Response
    {
        $result = $this->handleCommand(new GetListAsset());

        return new Response($result->serialize());
    }

    /**
     * Create new asset
     *
     * @param Request $request
     *
     * @OA\RequestBody(
     *     request="Asset",
     *     description="Asset",
     *     required=true,
     *     @OA\JsonContent(ref=@Model(type=Asset::class, groups={"Write"}))
     * )
     *
     * @OA\Response(
     *     response=201,
     *     description="Asset created"
     * )
     *
     * @OA\Response(
     *     response=422,
     *     description="Unprocessable entity error",
     *     @Model(type=ErrorModel::class)
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Data validation failed",
     *     @Model(type=ErrorModel::class)
     * )
     *
     * @Security(name="Authorization")
     * @return Response
     * @throws \App\Application\Helper\Exception\JsonDecodeException
     */
    public function create(Request $request): Response
    {
        $data =  Json::decode($request->getContent(), true);

        $this->validator->validate(
            $data,
            new CreateAssetConstraints()
        );
        $command = new CreateAsset($data['label'], $data['value'], $data['currency']);

        $this->handleCommand($command);

        return new Response('', Response::HTTP_CREATED);
    }

    /**
     * Delete asset
     *
     * @param int $id
     *
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="Asset id",
     *     required=true
     * )
     *
     * @OA\Response(
     *     response=204,
     *     description="Asset deleted"
     * )
     *
     * @OA\Response(
     *     response=404,
     *     description="Resource not found",
     *     @Model(type=ErrorModel::class)
     * )
     *
     * @Security(name="Authorization")
     * @return Response
     */
    public function delete(int $id): Response
    {
        $command = new DeleteAsset($id);

        $this->handleCommand($command);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
