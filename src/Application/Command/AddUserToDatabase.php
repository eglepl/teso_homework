<?php declare(strict_types = 1);

namespace App\Application\Command;

use App\Domain\Entity\User;
use App\Domain\User\UserRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddUserToDatabase extends Command
{
    const USER_EMAIL = 'teso@teso.com';
    const API_TOKEN = 'teso-api-key-256';

    /** @var string  */
    protected static $defaultName = "app:load-user";

    /** @var string */
    private $kernelEnvironment;

    /** @var UserRepositoryInterface */
    private $userRepository;

    /**
     * AddUserToDatabase constructor.
     *
     * @param UserRepositoryInterface $userRepository
     * @param string                  $kernelEnvironment
     */
    public function __construct(UserRepositoryInterface $userRepository, string $kernelEnvironment)
    {
        $this->userRepository = $userRepository;
        $this->kernelEnvironment = $kernelEnvironment;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Load User to db');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ('dev' !== $this->kernelEnvironment) {
            $io->error('Command can be run in DEV environment only');

            exit();
        }

        $user = (new User())
            ->setEmail(self::USER_EMAIL)
            ->setApiToken(self::API_TOKEN);

        $this->userRepository->save($user);

        return 0;
    }

}
