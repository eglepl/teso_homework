<?php declare(strict_types = 1);

namespace App\Application\EventSubscriber;

use App\Application\Exception\MultipleFieldsException;
use App\Domain\Error\ErrorModel;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ShowErrorsSubscriber implements EventSubscriberInterface
{

    private $serializer;

    /**
     * ShowErrorsSubscriber constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['returnErrorResponse', 5],
        ];
    }

    /**
     * @param ExceptionEvent $event
     */
    public function returnErrorResponse(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if (!$exception instanceof MultipleFieldsException) {
            return;
        }

        $errors = new ErrorModel($exception->getCode(), $exception->getMessage());
        $errors->setErrors($exception->getErrors());

        $jsonResponse = $this->serializer->serialize($errors, 'json');

        $response = new Response($jsonResponse, $exception->getCode());

        $event->setResponse($response);
    }
}
