<?php declare(strict_types = 1);

namespace App\Application\Validator;

use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface as SymfonyValidatorInterface;

class Validator implements ValidatorInterface
{
    const PATH_REPLACEMENT_KEY = 'pathReplacement';

    /** @var SymfonyValidatorInterface */
    protected $validator;

    /**
     * Validator constructor.
     *
     * @param SymfonyValidatorInterface $validator
     */
    public function __construct(SymfonyValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array                         $data
     * @param ValidatorConstraintsInterface $constraints
     *
     * @throws ValidatorViolationException
     */
    public function validate(
        array $data,
        ValidatorConstraintsInterface $constraints
    ): void {
        $fieldConstraints = $constraints->constraints();

        $violations = new ConstraintViolationList();

        $violations->addAll($this->validator->validate($data, $fieldConstraints));

        if (count($violations) !== 0) {
            throw new ValidatorViolationException($violations, $data);
        }
    }
}
