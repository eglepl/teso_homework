<?php declare(strict_types = 1);

namespace App\Application\Validator;

use App\Application\Exception\MultipleFieldsException;
use App\Domain\Error\Error;
use Symfony\Component\Validator\ConstraintViolationList;

class ValidatorViolationException extends MultipleFieldsException
{
    /** @var ConstraintViolationList */
    private $violations;

    /** @var array */
    private $data;

    /**
     * ValidatorViolationException constructor.
     *
     * @param ConstraintViolationList $violations
     * @param array                   $data
     */
    public function __construct(
        ConstraintViolationList $violations,
        array $data = []
    ) {
        $this->data = $data;
        $this->violations = $violations;

        $message = 'Data validation failed';
        $code = 400;

        parent::__construct($message, $code, null);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->violationsToErrors();
    }

    /**
     * @return ConstraintViolationList
     */
    public function getViolations(): ConstraintViolationList
    {
        return $this->violations;
    }

    /**
     * @return array
     */
    private function violationsToErrors(): array
    {
        $return = [];
        $errors = $this->violations;

        if ($errors instanceof ConstraintViolationList) {
            $i = 0;
            while ($errors->has($i)) {
                $constraint = $errors[$i]->getConstraint();
                $code = $errors[$i]->getCode();
                $return[] = new Error(
                    $errors[$i]->getMessage(),
                    $constraint::getErrorName($code),
                    $errors[$i]->getParameters()[Validator::PATH_REPLACEMENT_KEY] ?? $errors[$i]->getPropertyPath()
                );
                $i++;
            }
        }

        return $return;
    }
}
