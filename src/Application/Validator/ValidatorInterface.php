<?php declare(strict_types = 1);

namespace App\Application\Validator;

interface ValidatorInterface
{
    /**
     * @param array                         $data
     * @param ValidatorConstraintsInterface $constraints
     */
    public function validate(
        array $data,
        ValidatorConstraintsInterface $constraints
    ): void;
}
