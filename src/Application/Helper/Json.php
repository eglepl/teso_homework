<?php declare(strict_types = 1);

namespace App\Application\Helper;

use App\Application\Helper\Exception\JsonDecodeException;

class Json
{
    const UNEXPECTED_ERROR = "Unexpected error";

    /** @var array
     * PHP documentation: http://php.net/manual/en/function.json-last-error.php
     */
    protected static $messages = array(
        JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded',
        JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
        JSON_ERROR_SYNTAX => 'Syntax error',
    );

    /**
     * @param string $json
     * @param bool $assoc
     * @return object|array
     * @throws JsonDecodeException
     */
    public static function decode(string $json, bool $assoc = false)
    {
        $result = json_decode($json, $assoc);
        if (!is_null($result)) {
            return $result;
        }
        $error = static::$messages[json_last_error()] ?? self::UNEXPECTED_ERROR;

        throw new JsonDecodeException($error);
    }
}
