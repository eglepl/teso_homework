<?php declare(strict_types = 1);

namespace App\Application\Helper\Exception;

use App\Application\Exception\MultipleFieldsException;
use App\Domain\Error\Error;

class JsonDecodeException extends MultipleFieldsException
{
    /**
     * @var string
     */
    private $errorMessage;

    /**
     * JsonDecodeException constructor.
     * @param string $errorMessage
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct($errorMessage = 'Unexpected error', $code = 400, \Throwable $previous = null)
    {
        $this->errorMessage = $errorMessage;

        parent::__construct('Error occurred while decoding json', $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return [
            new Error($this->errorMessage, 'INVALID_JSON', '[body]')
        ];
    }
}
