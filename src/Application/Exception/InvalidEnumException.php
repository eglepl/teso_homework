<?php declare(strict_types = 1);

namespace App\Application\Exception;

use App\Domain\Error\Error;

class InvalidEnumException extends MultipleFieldsException
{
    private $enumName;

    public function __construct($enumName = '', $code = 0, \Throwable $previous = null)
    {
        $this->enumName = $enumName;

        parent::__construct('Invalid enum', 400, $previous);
    }

    public function getErrors(): array
    {
        return [
            new Error(
                sprintf('Invalid %s.', $this->enumName),
                'INVALID_ENUM',
                'PATH'
            )
        ];
    }
}
