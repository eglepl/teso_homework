<?php declare(strict_types=1);

namespace App\Application\Exception;

use App\Domain\Error\Error;
use Symfony\Component\HttpFoundation\Response;

class UnprocessableEntityException extends MultipleFieldsException
{
    /**
     * @var string
     */
    private $errorMessage;

    public function __construct(
        $errorMessage = '',
        $code = Response::HTTP_UNPROCESSABLE_ENTITY,
        \Throwable $previous = null
    ) {
        $this->errorMessage = $errorMessage;

        parent::__construct('Unprocessable entity', $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return [
            new Error($this->errorMessage, 'Unprocessable Entity', '[identifier]')
        ];
    }
}
