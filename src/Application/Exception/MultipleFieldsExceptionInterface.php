<?php declare(strict_types = 1);

namespace App\Application\Exception;

use App\Domain\Error\Error;

interface MultipleFieldsExceptionInterface
{
    /**
     * @return Error[]
     */
    public function getErrors(): array;
}
