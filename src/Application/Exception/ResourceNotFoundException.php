<?php declare(strict_types = 1);

namespace App\Application\Exception;

use App\Domain\Error\Error;

class ResourceNotFoundException extends MultipleFieldsException
{
    /**
     * @var string
     */
    private $resourceName;

    /**
     * ResourceNotFoundException constructor.
     * @param string $resourceName
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct($resourceName = '', $code = 404, \Throwable $previous = null)
    {
        $this->resourceName = $resourceName;

        parent::__construct('Resource not found', $code, $previous);
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return [
            new Error(sprintf('%s not found.', $this->resourceName), 'RESOURCE_NOT_FOUND', '[identifier]')
        ];
    }
}
