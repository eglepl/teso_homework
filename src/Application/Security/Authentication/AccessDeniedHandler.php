<?php declare(strict_types = 1);

namespace App\Application\Security\Authentication;

//use App\Model\Error\Error;
//use App\Model\Error\ErrorModel;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    /**
     * Handles an access denied failure.
     *
     * @return Response|null
     */
    public function handle(Request $request, AccessDeniedException $accessDeniedException)
    {
//        dd($accessDeniedException);
//        $data = $this->getErrorResponse(Response::HTTP_FORBIDDEN, 'Access Denied.');

//        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
        return new JsonResponse('papa', Response::HTTP_FORBIDDEN);
    }

    /**
     * @param int    $httpCode
     * @param string $errorMessage
     *
     * @return array
     */
//    private function getErrorResponse(int $httpCode, string $errorMessage): array
//    {
//        $error = new ErrorModel($httpCode, 'Access denied');
//        $error->addError(new Error($errorMessage, 'ACCESS_DENIED', ''));
//
//        return $error->toArray();
//    }
}
