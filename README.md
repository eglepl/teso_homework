## Initial Project set up

Just run init script in project root directory
`./initEnv`

## Accessible services
    - phpMyAdmin Database client
    http://localhost:8090
    - Project docummentation with all available endpoints
    http://localhost:8084/api/doc
    
## Authorization
    - to authorize to documentation enter X-AUTH-TOKEN: teso-api-key-256
